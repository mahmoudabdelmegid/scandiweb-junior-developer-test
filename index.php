<?php
require 'includes/header.php';
require 'includes/db.inc.php';
$result=mysqli_query($conn, "SELECT * FROM products");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="css/style.css"/>  
    <title>Product List</title>
</head>
<body>
    <header>
		<div Class="title">
			<p>Products List</p>
		</div>
		<hr>
        <div class="button">
			<button class="addBtn"><a href="ProductsAddingPage/ProductsAddingPage.php">ADD</a></button>
			<form action="includes/delete.inc.php" method="POST"> 
			<button class="delete-checkbox" type="submit" name="MassDelete">MASS DELETE</button>	
		</div>
	</header>
	<section>
        <?php 
     $query = "SELECT * FROM products ORDER BY PName ";
     $result = mysqli_query($conn,$query);
     if($row = mysqli_num_rows($result) > 0){
     ?>
	<div class="container3">
		<div class="row">    
        <?php foreach ($result as $res) {	?>   
    
                    <div id="row1">
                        <?php if ($res['PType'] == 'dvd') { ?>
                            <input class='checkMark' name='checkDelete[]' type='checkbox' value="<?php echo $res["PId"]; ?>"><br>
                            <?= $res['PSku'] ?><br>
                            <?= $res['PName'] ?><br>
                            <?= $res['PPrice'] ?> $<br>
                            Size: <?= $res['DSize'] ?> MB
            
            
                        <?php } else if ($res['PType'] == 'furniture') { ?>
                            <input class='checkMark' name='checkDelete[]' type='checkbox' value="<?php echo $res["PId"]; ?>"><br>
                            <?= $res['PSku'] ?><br>
                            <?= $res['PName'] ?><br>
                            <?= $res['PPrice'] ?> $<br>
                            Dimension: <?= $res['FDimension'] ?> 
                   
                        <?php } else { ?>
                            <input class='checkMark'name='checkDelete[]' type='checkbox' value="<?php echo $res["PId"]; ?>"><br>
                            <?= $res['PSku'] ?><br>
                            <?= $res['PName'] ?><br>
                            <?= $res['PPrice'] ?> $<br>
                            Weight: <?= $res['BWeight'] ?> KG 
                        <?php } ?>
                    </div>	

            <?php } } ?>
		</div>
	</div>
	
	</section>
	<footer class="footer">
	<hr id="footerLine">
<?php
	 
	include_once("includes/footer.php");
?>

    </footer>
    
</body>
</html>