<?php
require '../includes/header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="../css/style.css"/>  
    <title>Product Add</title>
</head>
<body>
    <header>
		<div Class="title">
			<p>Products Add</p>
		</div>
        <hr>
        <div class="button">
            <button class="cancelBtn">
                <a href="../index.php">Cancel</a>
            </button>			
            <form id="product_form" name="form" method="post" action="../includes/PAD.inc.php" onsubmit="return validateForm()">
            <button id="saveBtn" type="submit" class="btn" name="save" onClick="header('Location:index.php');">Save</button>
		</div>
    </header>
    <section>
        <div class="container">
            <div class="row">
                    <div class="form-group">
                        <label for="sku">SKU</label>
                        <input id="sku" type="text" class="form-control" name="sku">
                    </div><br>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name" type="text" class="form-control" name="name">
                    </div><br>
                    <div class="form-group">
                        <label for="price">Price $</label>
                        <input id="price" type="number" step="0.01" class="form-control" name="price">
                    </div><br>
                    <div id="productType" class="form-group">
                        <label for="type">Type Switcher</label>
                        <select class="form-control" id="type" name="type">
                        <option selected disabled value="">Type Switcher</option>
                        <option value='dvd'>DVD</option>
                        <option value='book'>Book</option>
                        <option value='furniture'>Furniture</option>
                        </select>
                    </div><br>
                    <div class="form-group selection" name="dvd" id="dvd" style="display: none;">
                        <label for="size">Size (MB)</label>
                        <input type="text" name="size" class="form-control w-50" id="size" aria-describedby="sizehelp"><br><br>
                        <small>Please provide size in MB format.</small>
                    </div>
                    <div class="form-group selection" name="book" id="book" style="display: none;">
                        <label for="weight">Weight (KG)</label>
                        <input type="text" name="weight" class="form-control w-50" id="weight" aria-describedby="weighthelp"><br><br>
                        <small>Please provide weight in Kg format.</small>
                    </div>
                    <div class="form-group selection" name="furniture" id="furniture" style="display: none;">
                        <label for="height">Height (CM)</label>
                        <input type="text" name="height" class="form-control w-50" id="height" aria-describedby="heighthelp"><br><br>
                        <label for="width">Width (CM)</label>
                        <input type="text" name="width" class="form-control w-50" id="width" aria-describedby="widthhelp"><br><br>
                        <label for="length">Length (CM)</label>
                        <input type="text" name="length" class="form-control w-50" id="length" aria-describedby="lengthhelp"><br><br>
                        <small>Please provide dimensions in Cm format.</small>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <footer class="footer">
        <hr id="footerLine">
        <?php
            include_once("../includes/footer.php");
        ?>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script> 
    <script type="text/javascript" src="TypeSwitcher.js"></script>
    <script type="text/javascript" src="Validation.js"></script>
</body>
</html>
