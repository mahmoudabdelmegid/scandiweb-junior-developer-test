function validateForm() {
    if (document.form.sku.value == '') {
        alert('Please Provide The Product Sku');
        return false;
    }
    if (document.form.name.value == '') {
        alert('Please Provide The Product name');
        return false;
    }
    if (document.form.price.value == '') {
        alert('Please Provide The Product Price');
        return false;
    }
    return true;
}