<?php
//Check if the user presses save
if (isset($_POST['save'])) {
    //DB Connection
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    require 'db.inc.php';
    
    //Calling the data that the user entered
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $type = $_POST['type'];
    $size = $_POST['size'];
    $weight = $_POST['weight'];
    $height = $_POST['height'];
    $width = $_POST['width'];
    $length = $_POST['length'];
    $dimension = $height . "x" . $width . "x" . $length;

    //Check if the user entered all data
    if (empty($sku) || empty($name) || empty($price) || empty($type)) {
        header("location:ProductsAddingPage/ProductsAddingPage.php?error=emptyfields&sku=".$sku."&name=".$name);
        exit();
    }else{
        $sql = "SELECT * FROM products WHERE PSku=?";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
           header("location:../ProductsAddingPage/ProductsAddingPage.php?error=sqlerror");
           exit(); 
        }else{
            mysqli_stmt_bind_param($stmt, "s", $sku);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if ($resultCheck > 0) {
                echo "<script>
                alert('Please Enter A Unique Sku'); 
                window.location.href = '../ProductsAddingPage/ProductsAddingPage.php';
                </script>";
                //header("location:../ProductsAddingPage/ProductsAddingPage.php?error=skuIsAlreadyInTheDatabase");
                exit(); 
            }else{

                //Insert all the data to the database
                if ($type == 'dvd') {
                $sql = "INSERT INTO products (PSku, PName, PPrice, PType, DSize) VALUES (?, ?, ?, ?, ?)";
                $stmt = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($stmt, $sql)) {
                        header("location:../ProductsAddingPage/ProductsAddingPage.php?error=sqlerror2");
                        exit(); 
                    }else{
                    
                        //insert data to the table
                        mysqli_stmt_bind_param($stmt, "sssss", $sku, $name, $price, $type, $size);
                        mysqli_stmt_execute($stmt);
                        header("location:../ProductsAddingPage/ProductsAddingPage.php?add=success");
                        exit();         
                    }
                }
                elseif ($type == 'book') {
                    $sql = "INSERT INTO products (PSku, PName, PPrice, PType, BWeight) VALUES (?, ?, ?, ?, ?)";
                    $stmt = mysqli_stmt_init($conn);
                        if (!mysqli_stmt_prepare($stmt, $sql)) {
                            header("location:../ProductsAddingPage/ProductsAddingPage.php?error=sqlerror3");
                            exit(); 
                        }else{
                        
                            //insert data to the table
                            mysqli_stmt_bind_param($stmt, "sssss", $sku, $name, $price, $type, $weight);
                            mysqli_stmt_execute($stmt);
                            header("location:../ProductsAddingPage/ProductsAddingPage.php?add=success");
                            exit();         
                        }
                }else{
                    $sql = "INSERT INTO products (PSku, PName, PPrice, PType, FDimension) VALUES (?, ?, ?, ?, ?)";
                    $stmt = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($stmt, $sql)) {
                        header("location:../ProductsAddingPage/ProductsAddingPage.php?error=sqlerror4");
                        exit(); 
                    }else{
                    
                        //insert data to the table
                        mysqli_stmt_bind_param($stmt, "sssss", $sku, $name, $price, $type, $dimension);
                        mysqli_stmt_execute($stmt);
                        header("location:../ProductsAddingPage/ProductsAddingPage.php?add=success");
                        exit();         
                    }
                }
            }
    
//end of stmt & conn
mysqli_stmt_close($stmt);
mysqli_close($conn);
        }
    }
}else{
    header("location:../ProductsAddingPage/ProductsAddingPage.php?add=error5");
    exit();   
}


