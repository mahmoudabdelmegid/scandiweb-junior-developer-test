<?php
$servername = "localhost";
$dbUsername = "root";
$dbPassword = "";
$dbName = "scandiweb test";


$conn = mysqli_connect($servername, $dbUsername, $dbPassword, $dbName);

if(!$conn){
    die("connection failed: ".mysqli_connect_error());
}


/* class dbh {
    private $servername;
    private $dbUsername;
    private $dbPassword;
    private $dbName;

    protected function connect() {
        $this->servername = "localhost";
        $this->dbUsername = "root";
        $this->dbPassword = "";
        $this->dbName = "scandiweb test";

        $conn = new mysqli($this->servername, $this->dbUsername, $this->dbPassword, $this->dbName );
        return $conn;
    }
} */